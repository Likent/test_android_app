package com.mycompany.my.test_android_app.di

import android.content.Context
import android.location.Geocoder
import android.location.LocationManager
import com.mycompany.my.test_android_app.BuildConfig
import com.mycompany.my.test_android_app.network.Api
import com.mycompany.my.test_android_app.repository.TrackingRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun providesRetrofitInstance(): Retrofit {
            val okHttpClient = OkHttpClient.Builder().build()
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

    @Provides
    @Singleton
    fun providesApiService(retrofit: Retrofit): Api =
        retrofit.create(Api::class.java)

    @Provides
    @Singleton
    fun provideLocationManager(@ApplicationContext context: Context): LocationManager{
        return context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @Provides
    @Singleton
    fun provideGeocode(@ApplicationContext context: Context) = Geocoder(context, Locale.getDefault())

    @ExperimentalCoroutinesApi
    @Provides
    @Singleton
    fun provideTrackingRepository(@ApplicationContext context: Context) = TrackingRepository(provideLocationManager(context), provideGeocode(context))
}