package com.mycompany.my.test_android_app.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.mycompany.my.test_android_app.models.weather.WeatherResponse
import com.mycompany.my.test_android_app.models.Resource
import com.mycompany.my.test_android_app.repository.TrackingRepository
import com.mycompany.my.test_android_app.repository.WeatherRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class WeatherViewModel @ViewModelInject constructor(private val weatherRepository: WeatherRepository, private val trackingRepository: TrackingRepository) : ViewModel() {

    private val weatherData = MutableLiveData<Resource<WeatherResponse>>()

    fun getWeatherData(): LiveData<Resource<WeatherResponse>> {
        return weatherData
    }

    init {
        updateData()
    }

    fun updateData(){
        weatherData.value = Resource.loading(data = null)
        viewModelScope.launch {
            try {
                val location = trackingRepository.getLocation()
                weatherData.value = Resource.success(weatherRepository.getWeatherData(location.latitude, location.longitude))
            }catch (e: Exception){
                Resource.error(null, e.localizedMessage ?: "Error")
            }
        }
    }

    fun getCityName() = trackingRepository.getCityNameData()

}