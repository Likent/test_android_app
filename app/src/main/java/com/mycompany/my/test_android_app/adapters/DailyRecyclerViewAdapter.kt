package com.mycompany.my.test_android_app.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mycompany.my.test_android_app.R
import com.mycompany.my.test_android_app.databinding.RowWeatherDayBinding
import com.mycompany.my.test_android_app.models.weather.Daily

class DailyRecyclerViewAdapter(items: List<Daily>, selectedListener: (Daily)-> Unit) : RecyclerView.Adapter<DailyRecyclerViewAdapter.DailyViewHolder>() {

    private val dailyList: List<Daily> = items
    private var selectedPosition = 0
    val itemSelectedListener = selectedListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DailyViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_weather_day, parent, false)
    )

    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        holder.binding.data = dailyList[position]
        if(position == selectedPosition){
            holder.binding.tvRowDay.setTextColor(ContextCompat.getColor(holder.binding.root.context, R.color.sky))
            holder.binding.tvRowDayTemperature.setTextColor(ContextCompat.getColor(holder.binding.root.context, R.color.sky))
            holder.binding.ivRowDayWeatherIcon.setColorFilter(ContextCompat.getColor(holder.binding.root.context, R.color.sky))
            holder.binding.root.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.light_grey))
        }else{
            holder.binding.tvRowDay.setTextColor(ContextCompat.getColor(holder.binding.root.context, R.color.black))
            holder.binding.tvRowDayTemperature.setTextColor(ContextCompat.getColor(holder.binding.root.context, R.color.black))
            holder.binding.ivRowDayWeatherIcon.setColorFilter(ContextCompat.getColor(holder.binding.root.context, R.color.black))
            holder.binding.root.setBackgroundColor(ContextCompat.getColor(holder.binding.root.context, R.color.white))
        }
        holder.binding.root.setOnClickListener {
            selectedPosition.also {
                selectedPosition = position
                notifyItemChanged(position)
                notifyItemChanged(it)
                itemSelectedListener(dailyList[selectedPosition])
            }
        }
    }

    override fun getItemCount() = dailyList.size

    class DailyViewHolder(rowBinding: RowWeatherDayBinding): RecyclerView.ViewHolder(rowBinding.root) {
        val binding = rowBinding

    }
}