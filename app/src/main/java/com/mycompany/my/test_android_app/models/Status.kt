package com.mycompany.my.test_android_app.models

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}