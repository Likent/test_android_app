package com.mycompany.my.test_android_app.models.weather

import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("lon")
    val lon: Double,
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("hourly")
    val hourlyList: List<Hourly>,
    @SerializedName("daily")
    val dailyList: List<Daily>
)