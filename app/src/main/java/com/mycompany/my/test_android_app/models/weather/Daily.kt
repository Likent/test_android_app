package com.mycompany.my.test_android_app.models.weather

import com.google.gson.annotations.SerializedName

data class Daily(
    @SerializedName("dt")
    val dt: Long,
    @SerializedName("humidity")
    val humidity: Int,
    @SerializedName("wind_speed")
    val wind_speed: Double,
    @SerializedName("wind_deg")
    val wind_deg: Int,
    @SerializedName("weather")
    val weather: List<Weather>,
    @SerializedName("temp")
    val temp: Temperature
)
