package com.mycompany.my.test_android_app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.mycompany.my.test_android_app.BuildConfig
import com.mycompany.my.test_android_app.R
import com.mycompany.my.test_android_app.databinding.FragmentMapsBinding
import com.mycompany.my.test_android_app.viewmodels.MapViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MapFragment: Fragment(), PlaceSelectionListener {

    private val callback = OnMapReadyCallback { googleMap ->
        viewModel.getCurrentLocation().observe(viewLifecycleOwner){
            googleMap.clear()
            googleMap.addMarker(MarkerOptions().position(it))
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(it, 10f))
        }
        googleMap.setOnMapClickListener {
            googleMap.clear()
            googleMap.addMarker(MarkerOptions().position(it))
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(it, 10f))
            changeLocation(it)
        }
    }

    private lateinit var binding: FragmentMapsBinding
    private lateinit var placesClient: PlacesClient
    private val viewModel: MapViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false)
        binding.mapView.onCreate(savedInstanceState)
        try {
            MapsInitializer.initialize(requireContext().applicationContext)
        }catch (e: Exception){
            e.printStackTrace()
        }

        if (!Places.isInitialized()) {
            context?.let {
                Places.initialize(it.applicationContext, BuildConfig.GOOGLE_PLACE_KEY)
            }
        }
        placesClient = Places.createClient(requireContext())
        val autocompleteFragment = childFragmentManager.findFragmentById(binding.placeAutocompleteFragment.id) as AutocompleteSupportFragment
        autocompleteFragment.setTypeFilter(TypeFilter.CITIES)
        autocompleteFragment.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS
            )
        )
        autocompleteFragment.setOnPlaceSelectedListener(this)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mapView.getMapAsync(callback)
    }

    private fun changeLocation(latLng: LatLng){
        viewModel.changeLocation(latLng)
        findNavController().popBackStack()
    }



    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }


    override fun onPlaceSelected(place: Place) {
        place.latLng?.let { changeLocation(it) }
    }

    override fun onError(status: Status) {
    }
}