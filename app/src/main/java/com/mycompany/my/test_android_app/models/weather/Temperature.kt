package com.mycompany.my.test_android_app.models.weather

import com.google.gson.annotations.SerializedName

data class Temperature(
    @SerializedName("day")
    val day: Double,
    @SerializedName("min")
    val min: Double,
    @SerializedName("max")
    val max: Double,
)
