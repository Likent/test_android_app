package com.mycompany.my.test_android_app.repository

import android.annotation.SuppressLint
import android.location.Geocoder
import android.location.LocationManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class TrackingRepository @Inject constructor(locationManager: LocationManager, geocoder: Geocoder) {
    val mLocationManager: LocationManager = locationManager
    val mGeocoder: Geocoder = geocoder

    private val locationData = MutableLiveData<LatLng>()
    private val cityNameData = MutableLiveData<String>()


    fun getLocationData(): LiveData<LatLng> {
        return locationData
    }

    fun getCityNameData(): LiveData<String>{
        return cityNameData
    }

    @SuppressLint("MissingPermission")
    fun getLocation(): LatLng {
        if ( locationData.value != null ) return  locationData.value!!
        val gpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val networkLocation =
            mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
         when {
            gpsLocation != null -> {
                locationData.value =LatLng(gpsLocation.latitude, gpsLocation.longitude)
            }
            networkLocation != null -> {
                locationData.value =LatLng(networkLocation.latitude, networkLocation.longitude)
            }
            else -> {
                locationData.value = LatLng(47.0, 35.0)
                cityNameData.value = "Запорожье"
            }
        }
        cityNameData.value = getCityName()
        return  locationData.value!!
    }

    private fun getCityName(): String{
        val adresses = mGeocoder.getFromLocation( locationData.value!!.latitude,  locationData.value!!.longitude, 1)
        if(adresses.isEmpty()) return ""
        return adresses[0].getAddressLine(0).split(",")[1]
    }

    fun setLocation(latLng: LatLng){
        locationData.value = latLng
        cityNameData.value = getCityName()
    }
}