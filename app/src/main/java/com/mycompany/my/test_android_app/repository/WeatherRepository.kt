package com.mycompany.my.test_android_app.repository

import com.mycompany.my.test_android_app.network.ApiImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val apiImpl: ApiImpl) {

    @ExperimentalCoroutinesApi
    suspend fun getWeatherData(lat: Double, lon: Double) = apiImpl.getWeatherData(lat, lon)
}