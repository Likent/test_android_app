package com.mycompany.my.test_android_app.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.google.android.gms.maps.model.LatLng
import com.mycompany.my.test_android_app.repository.TrackingRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MapViewModel @ViewModelInject constructor(private val trackingRepository: TrackingRepository) :
    ViewModel() {

    fun getCurrentLocation(): LiveData<LatLng> {
        return trackingRepository.getLocationData()
    }

    fun changeLocation(latLng: LatLng) {
        trackingRepository.setLocation(latLng)
    }
}