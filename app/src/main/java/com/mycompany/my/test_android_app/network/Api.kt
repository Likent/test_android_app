package com.mycompany.my.test_android_app.network

import com.mycompany.my.test_android_app.BuildConfig
import com.mycompany.my.test_android_app.models.weather.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("onecall")
    suspend fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") key: String = BuildConfig.API_KEY,
        @Query("exclude") exclude: String = "minutely,current",
        @Query("units") units: String = "metric") : WeatherResponse

}