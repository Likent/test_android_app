package com.mycompany.my.test_android_app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.mycompany.my.test_android_app.R
import com.mycompany.my.test_android_app.adapters.DailyRecyclerViewAdapter
import com.mycompany.my.test_android_app.adapters.HourlyRecyclerViewAdapter
import com.mycompany.my.test_android_app.databinding.FragmentWeatherBinding
import com.mycompany.my.test_android_app.models.Status
import com.mycompany.my.test_android_app.viewmodels.WeatherViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class WeatherFragment : Fragment(), Toolbar.OnMenuItemClickListener {

    private lateinit var binding: FragmentWeatherBinding
    private val viewModel: WeatherViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false)
        setHasOptionsMenu(true)
        binding.toolbar.inflateMenu(R.menu.menu_weather)
        binding.toolbar.setOnMenuItemClickListener(this)
        NavigationUI.setupWithNavController(binding.toolbar, findNavController())
        return binding.root
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        findNavController().navigate(WeatherFragmentDirections.actionWeatherFragmentToMapFragment())
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getWeatherData().observe(viewLifecycleOwner) {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { response ->
                            binding.weather = response.dailyList[0]
                            binding.rvDaily.adapter =
                                DailyRecyclerViewAdapter(response.dailyList) { selectedItem ->
                                    binding.weather = selectedItem
                                    binding.notifyChange()
                                }
                            binding.rvHourly.adapter =
                                HourlyRecyclerViewAdapter(response.hourlyList)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> Toast.makeText(
                        requireContext(),
                        "Loading",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        viewModel.getCityName().observe(viewLifecycleOwner){
            binding.toolbar.title = it
            viewModel.updateData()
        }
    }


}