package com.mycompany.my.test_android_app.adapters

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.mycompany.my.test_android_app.R
import java.sql.Timestamp
import java.util.*

@BindingAdapter("app:time")
fun timestampToTime(textView: TextView, timestamp: Long){
    val calendar = GregorianCalendar.getInstance()
    calendar.time = Timestamp(timestamp * 1000)
    textView.text = String.format(textView.context.getString(R.string.time_format), calendar.get(Calendar.HOUR_OF_DAY))
}

@BindingAdapter("app:dayName")
fun timestampToDay(textView: TextView, timestamp: Long){
    val calendar = GregorianCalendar.getInstance()
    calendar.time = Timestamp(timestamp * 1000)
    textView.text = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())!!.toUpperCase(
        Locale.getDefault())
}

@BindingAdapter("app:dayAndMonth")
fun timestampToDayAndMonth(textView: TextView, timestamp: Long){
    val calendar = GregorianCalendar.getInstance()
    val resources = textView.context.resources
    calendar.time = Timestamp(timestamp * 1000)
    textView.text = String.format(resources.getString(R.string.day_month_format),
        calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())!!.toUpperCase(
            Locale.getDefault()),
        calendar.get(Calendar.DAY_OF_MONTH),
        calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()))

}

@BindingAdapter("app:weatherImg")
fun loadWeatherImage(imageView: ImageView, weatherImageId: Int){
    when(weatherImageId){
        200, 201, in 500..599 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_white_day_rain))
        }
        202 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_white_day_shower))
        }
        211 -> {
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_white_day_thunder))
        }
        in 800..805 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_white_day_cloudy))
        }
        else ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_white_day_bright))
        }
    }

}

@BindingAdapter("app:directionImg")
fun loadDirectionImage(imageView: ImageView, windDirectionDegree: Int){
    when (windDirectionDegree){
        in 338..360, in 0..23 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_n))
        }
        in 23..68 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_ne))
        }
        in 68..113 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_e))
        }
        in 113..158 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_se))
        }
        in 158..203 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_s))
        }
        in 203..248 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_ws))
        }
        in 248..293 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_w))
        }
        in 293..338 ->{
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, R.drawable.ic_icon_wind_wn))
        }
    }
}