package com.mycompany.my.test_android_app.network

import com.mycompany.my.test_android_app.models.weather.WeatherResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class ApiImpl @Inject constructor(private val api: Api) {
    @ExperimentalCoroutinesApi
    suspend fun getWeatherData(lat: Double, lon: Double): WeatherResponse {
        return api.getWeather(lat, lon)
    }
}