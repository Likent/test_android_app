package com.mycompany.my.test_android_app.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mycompany.my.test_android_app.R
import com.mycompany.my.test_android_app.databinding.RowWeatherHourBinding
import com.mycompany.my.test_android_app.models.weather.Hourly

class HourlyRecyclerViewAdapter(items: List<Hourly>):
    RecyclerView.Adapter<HourlyRecyclerViewAdapter.HourlyViewHolder>() {
    private val dailyList: List<Hourly> = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HourlyViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.row_weather_hour, parent, false)
    )

    override fun onBindViewHolder(holder: HourlyViewHolder, position: Int) {
        holder.binding.data = dailyList[position]
    }

    override fun getItemCount() = dailyList.size

    class HourlyViewHolder(rowBinding: RowWeatherHourBinding): RecyclerView.ViewHolder(rowBinding.root) {
        val binding = rowBinding
    }
}